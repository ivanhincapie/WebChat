
//const app = express();

const express = require('express');
const path = require('path');
const port = 3000;
const host = 'localhost';
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

//app.use('/', express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/public/index.html');
});

app.get('/prueba',(req,res)=>{
    res.send ('Hello World!');
});

server.listen(port,host,()=>{
    console.log(`listening in http://${host}:${port}`);
});  

io.on('connection', function (socket) {
  console.log(`Otro usuarios conectado`);
  socket.emit('Room 1', { hello: 'world' });
  socket.on('Room Test', function (data) {
    console.log(data);
  });
});



/*
const express = require('express');
const path = require('path');
const app = express();
const port = 5000;
const host = '0.0.0.0';

app.use('/', express.static(path.join(__dirname, 'public')));

app.get('/pruebas', (req, res)=> { 
    var hora = Date();
    res.send('Hola Mundo, la hora es: '  + hora);
});


app.listen(port, host, ()=>{
    console.log(`Listening in http://${host}:${port}`);
}); */